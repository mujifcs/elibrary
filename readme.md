# Library System

## Web Application in Laravel 5.7


## Setup
1. Clone the project from -[repo](https://bitbucket.org/mujifcs/elibrary)

2. Go to the application folder using cd

3. Run `composer install` on your cmd or terminal

4. Copy .env.example file to .env on root folder

5. Open your .env file and change the database name (DB_DATABASE) to whatever you have, username (DB_USERNAME) and password (DB_PASSWORD) field correspond to your configuration. 

6. Run `php artisan migrate`

7. Run `php artisan db:seed` for initial data
    admin username: admin@test.com
    admin password: password
    
8. Run `php artisan serve` to run it locally

9. Your app is ready!
