
<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">

    <head>
        @include('admin.layouts.head')
    </head>

    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <!-- Main Header -->
            @include('admin.layouts.header')

            <!-- Left side column. contains the logo and sidebar -->
            @include('admin.layouts.leftside')





            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">

                    <h1>
                        @yield('content-header')
                    </h1>

                </section>


                <!-- Main content -->
                <section class="content container-fluid">

                    @include('admin.layouts.alerts')

                    @yield('content')

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->





            @include('admin.layouts.footer')
        </div>
        <!-- ./wrapper -->

        <!-- REQUIRED JS SCRIPTS -->
        @include('admin.layouts.scripts')
    </body>
</html>
