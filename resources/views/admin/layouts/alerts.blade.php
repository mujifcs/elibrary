

@if (session('status'))
    <div class="callout alert alert-dismissible callout-info">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

        <h4>
            {{ session('status') }}
        </h4>
        
    </div>
@endif



@if (count($errors))
    <div class="callout alert alert-dismissible callout-danger">

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
