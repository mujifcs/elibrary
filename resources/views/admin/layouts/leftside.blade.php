<aside class="main-sidebar">

<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">

  <!-- Sidebar user panel (optional) -->
  <div class="user-panel">
    <div class="pull-left image">
      <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
    </div>
    <div class="pull-left info">
      <p>{{ auth()->user()->name }}</p>
    </div>
  </div>

  <!-- search form (Optional) -->
  <!-- <form action="#" method="get" class="sidebar-form">
    <div class="input-group">
      <input type="text" name="q" class="form-control" placeholder="Search...">
      <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
          </button>
        </span>
    </div>
  </form> -->
  <!-- /.search form -->

  <!-- Sidebar Menu -->
  <ul class="sidebar-menu" data-widget="tree">
      <li ><a href="{{ URL::to('admin')}}"><i class="fa fa-home"></i> <span>Admin Home</span></a></li>
      <li><a href="{{ URL::to('admin/racks')}}"><i class="fa fa-folder"></i> <span>Manage racks</span></a></li>
      <li><a href="{{ URL::to('admin/books')}}"><i class="fa fa-book"></i> <span>Manage books</span></a></li>
  </ul>
  <!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->
</aside>
