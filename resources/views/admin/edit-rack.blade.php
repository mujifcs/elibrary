@extends('admin.layouts.master')


@section('headlinks')

  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">

@endsection


@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box box-danger ">

            <div class="box-header with-border">
                <h3 class="box-title">Update Rack: {{$rack->name}}</h3>
            </div>


            <div class="box-body">

                <form class="form-inline pull-left" method="POST"  action="{{ URL::to('admin/racks/'.$rack->id) }}">
                    @csrf
                    @method('PATCH')
                    <div class="form-group">
                        <label for="name">Rack Name:</label>
                        <input type="text" class="form-control" id="name" name="name" required value="{{ $rack->name }}">
                    </div>

                    <button type="submit" class="btn btn-primary">Update Name</button>
                </form>

                <form class="form pull-right" method="POST"  action="{{ URL::to('admin/racks/'.$rack->id) }}">
                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger">Delete Rack</button>
                </form>


            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-xs-12">
        <div class="box">

            <div class="box-header with-border">
                <h3 class="box-title">Books in {{$rack->name}} </h3>
            </div>
            <!-- /.box-header -->

            <div class="box-body">
                <table id="example1" class="table table-bordered table-hover text-center">

                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Year</th>
                            <th>Edit</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($rack->books as $book)
                            <tr >
                                <td>{{$book->id}}</td>
                                <td>{{$book->title}}</td>

                                <td >{{$book->auhtor}}</td>
                                <td >{{$book->pub_year}}</td>
                                <td><a href="{{ URL::to('admin/books/'.$book->id.'/edit')}}"><i class="ion-edit"></i></a></td>
                            </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>



@endsection



@section('scripts')
<!-- DataTables -->
<script src="{{ asset('/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script>
    $(function () {
        $('#example1').DataTable()
    });

</script>

@endsection
