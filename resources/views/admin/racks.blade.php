@extends('admin.layouts.master')


@section('headlinks')

  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">

@endsection


@section('content')


<div class="row">
    <div class="col-xs-12">
        <div class="box box-success ">

            <div class="box-header with-border">
                <h3 class="box-title">Add Rack</h3>
            </div>


            <div class="box-body">

                <form class="form-inline" method="POST" action="{{ URL::to('admin/racks')}}">
                    @csrf
                    <div class="form-group">
                        <label for="name">Rack Name:</label>
                        <input type="text" class="form-control" id="name" name="name" required value="{{ old('name')}}">
                    </div>
                    <button type="submit" class="btn btn-primary">Add Rack</button>
                </form>

            </div>
        </div>
    </div>
</div>





<div class="row">
    <div class="col-xs-12">
        <div class="box">

            <div class="box-header with-border">
                <h3 class="box-title">Racks Record </h3>
            </div>
            <!-- /.box-header -->

            <div class="box-body">
                <table id="example1" class="table table-bordered table-hover text-center">

                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Books</th>
                            <th>Edit</th>
                        </tr>
                    </thead>

                    <tbody>
                        @if($racks)
                            @foreach($racks as $rack)
                                <tr >
                                    <td>{{$rack->id}}</td>
                                    <td>{{$rack->name}}</td>

                                    <td >{{$rack->books()->count()}}</td>
                                    <td><a href="{{ URL::to('admin/racks/'.$rack->id.'/edit')}}"><i class="ion-edit"></i></a></td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>

                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>



@endsection



@section('scripts')
<!-- DataTables -->
<script src="{{ asset('/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script>
    $(function () {
        $('#example1').DataTable()
    });

</script>

@endsection
