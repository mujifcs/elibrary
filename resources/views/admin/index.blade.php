@extends('admin.layouts.master')

@section('content')

<div class="col-lg-4 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-yellow">
    <div class="inner">
        <h3>{{ $rackCount }}</h3>

        <h4>Racks</h4>
    </div>
    <a href="{{ URL::to('admin/racks')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>


<div class="col-lg-4 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-yellow">
    <div class="inner">
        <h3>{{ $bookCount }}</h3>
        <h4>Books</h4>
    </div>
    <a href="{{ URL::to('admin/books')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>

@endsection
