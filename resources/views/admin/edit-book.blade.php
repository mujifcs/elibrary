@extends('admin.layouts.master')


@section('headlinks')

  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">

@endsection


@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box box-success ">

            <div class="box-header with-border">
                <h3 class="box-title">Book: {{ucwords($book->title)}}</h3>
                <form class="form pull-right" method="POST"  action="{{ URL::to('admin/books/'.$book->id) }}">
                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger">Delete Book</button>
                </form>
            </div>


            <div class="box-body">

                <form class="form" method="POST" action="{{ URL::to('admin/books/'.$book->id)}}">

                    @csrf
                    @method('PATCH')
                    <div class="form-group">
                        <label for="title">Book Title</label>
                        <input type="text" class="form-control" id="title" name="title" required value="{{ $book->title }}">
                    </div>
                    <div class="form-group">
                        <label for="author">Author</label>
                        <input type="text" class="form-control" id="author" name="author" required value="{{ $book->author }}">
                    </div>
                    <div class="form-group">
                        <label for="pub_year">Publish Year</label>
                        <input type="number" min="1901" max="2155" class="form-control" id="pub_year"  name="pub_year" required value="{{ $book->pub_year }}">
                    </div>

                    <div class="form-group">
                        <label for="rack">Rack</label>
                        <select  class="form-control" id="rack" name="rack" required>
                            @foreach( $racks as $rack)
                                <option @if($book->rack_id === $rack->id) {{'selected'}} @endif value = {{ $rack->id }}>{{$rack->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <button type="submit" class="btn btn-primary">Update Book Details</button>
                </form>

            </div>
        </div>
    </div>
</div>


@endsection

