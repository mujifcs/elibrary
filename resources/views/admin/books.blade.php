@extends('admin.layouts.master')


@section('headlinks')

  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">

@endsection


@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box box-success ">

            <div class="box-header with-border">
                <h3 class="box-title">Add New Book</h3>
            </div>


            <div class="box-body">

                <form class="form" method="POST" action="{{ URL::to('admin/books')}}">

                    @csrf
                    <div class="form-group">
                        <label for="title">Book Title</label>
                        <input type="text" class="form-control" id="title" name="title" required value="{{ old('title')}}">
                    </div>
                    <div class="form-group">
                        <label for="author">Author</label>
                        <input type="text" class="form-control" id="author" name="author" required value="{{ old('author')}}">
                    </div>
                    <div class="form-group">
                        <label for="pub_year">Publish Year</label>
                        <input type="number" min="1901" max="2155" class="form-control" id="pub_year"  name="pub_year" required value="{{ old('pub_year')}}">
                    </div>

                    <div class="form-group">
                        <label for="rack">Rack</label>
                        <select  class="form-control" id="rack" name="rack" required>
                            @foreach( $racks as $rack)
                                <option  value = {{ $rack->id }}>{{$rack->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <button type="submit" class="btn btn-primary">Add Book</button>
                </form>

            </div>
        </div>
    </div>
</div>





<div class="row">
    <div class="col-xs-12">
        <div class="box">

            <div class="box-header with-border">
                <h3 class="box-title">Books Record</h3>
            </div>
            <!-- /.box-header -->

            <div class="box-body">
                <table id="example1" class="table table-bordered table-hover text-center">

                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Year</th>
                            <th>Rack</th>
                            <th>Edit</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($books as $book)
                            <tr >
                                <td>{{$book->id}}</td>
                                <td>{{ucwords($book->title)}}</td>

                                <td >{{$book->author}}</td>
                                <td >{{$book->pub_year}}</td>
                                <td>{{$book->rack->name}}
                                <td><a href="{{ URL::to('admin/books/'.$book->id.'/edit')}}"><i class="ion-edit"></i></a></td>
                            </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>




@endsection



@section('scripts')
<!-- DataTables -->
<script src="{{ asset('/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script>
    $(function () {
        $('#example1').DataTable()
    });

</script>

@endsection
