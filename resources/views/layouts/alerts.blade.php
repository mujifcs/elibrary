@if (count($errors))
    <div class="container">
        <div class="alert alert-dismissible alert-danger">

            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif


@if (session('status'))
    <div class="container">
        <div class="alert alert-dismissible alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <h4>
                {{ session('status') }}
            </h4>

        </div>
    </div>
@endif
