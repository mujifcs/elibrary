
<form method="GET" action="{{URL::to('books/search')}}" class="form-inline">
    <input class="form-control" type="text" name="term"
        placeholder="Search book by title, author or year" aria-label="Search">

    <button class="btn btn-default" type="submit">Search</button>

</form>
