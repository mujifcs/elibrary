@extends('layouts.app')


@section('content')

<div class="container">

    <div class="row justify-content-center">
        <div class="col-10" >
            <div class="card ">
                <h4 class="card-header">
                    {{ucwords($book->title)}}
                </h4>

                <div class="card-body">
                    <div class="card-title">
                        Book is in rack: <a href="{{ URL::to('racks/'.$rack->id)}}">{{ ucwords($rack->name) }}</a>
                    </div>
                    <hr>
                    <div class="text-muted text-sm w-100"><small>Published Year: {{ $book->pub_year }}</small></div>
                    <div class="text-muted text-smw-100"><small>Author: {{ $book->author }}</small></div>

                </div>
            </div>
        </div>
    </div>

</div>

@endsection
