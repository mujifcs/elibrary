@extends('layouts.app')


@section('content')

<div class="container">

    <div class="row justify-content-center">
        <div class="col-10" >
            <div class="card ">

                <div class="card-header">
                    Results for: <strong>{{ $term }}</strong>
                </div>
                <div class="card-body">
                    <div class="card-title">Books matched: {{$books->count()}}</div>
                    <hr>

                    @if($books->count())
                        <div class="list-group">
                            @foreach( $books as $book)
                                <a href="{{ URL::to('racks/'.$book->rack_id.'/books/'.$book->id)}}"
                                    class="list-group-item list-group-item-action flex-column align-items-start">
                                    <h5 class="w-100">{{ucwords($book->title)}}</h5>
                                    <div class="text-muted text-sm w-100"><small>Published Year: {{ $book->pub_year }}</small></div>
                                    <div class="text-muted text-smw-100"><small>Author: {{ $book->author }}</small></div>
                                </a>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
