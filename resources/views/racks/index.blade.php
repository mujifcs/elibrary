@extends('layouts.app')


@section('content')

<div class="container">

    <div class="row">
        @foreach ($racks as $rack)
            <div class="col-sm-4 p-2" >
                <div class="card ">
                    <h4 class="card-header">
                        {{ ucwords($rack->name) }}
                    </h4>
                    <div class="card-body text-center">
                        <p class="card-text">Books: {{$rack->books()->count()}}</p>
                        <a href="{{ URL::to('racks/'.$rack->id)}}" class="btn btn-primary">View rack</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

@endsection
