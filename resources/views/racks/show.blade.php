@extends('layouts.app')


@section('content')

<div class="container">

    <div class="row justify-content-center">
        <div class="col-10" >
            <div class="card ">
                <h4 class="card-header">
                    {{ ucwords($rack->name) }}
                </h4>

                <div class="card-body">
                    <h5 class="card-title">Books availabe: {{$rack->books()->count()}}</h5>
                    <hr>

                    @if($rack->books()->count())
                        <div class="list-group">
                            @foreach( $rack->books as $book)
                                <a href="{{ URL::to('racks/'.$rack->id.'/books/'.$book->id)}}"
                                    class="list-group-item list-group-item-action flex-column align-items-start">
                                    <h5 class="w-100">{{ucwords($book->title)}}</h5>
                                    <div class="text-muted text-sm w-100"><small>Published Year: {{ $book->pub_year }}</small></div>
                                    <div class="text-muted text-smw-100"><small>Author: {{ $book->author }}</small></div>
                                </a>
                            @endforeach
                        </div>

                    @endif
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
