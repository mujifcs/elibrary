<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/home', function () {
    return redirect()->route('home');
});

Auth::routes();


Route::middleware(['auth'])->group(function () {
    Route::get('/racks', 'RacksController@index');
    Route::get('/racks/{rack}', 'RacksController@show');

    Route::get('/racks/{rack}/books/{book}', 'BooksController@show');

    Route::get('/books/search', 'BooksController@search');

});


// admin protected routes
Route::middleware(['auth', 'admin'])->prefix('admin')->group(function () {

    Route::get('/', function () {
        $rackCount = \App\Rack::all()->count();
        $bookCount = \App\Book::all()->count();

        return view('admin.index', compact('rackCount','bookCount'));
    });

    Route::get('/racks', 'RacksController@adminIndex');
    Route::post('/racks', 'RacksController@store');
    Route::get('/racks/{rack}/edit', 'RacksController@edit');
    Route::patch('/racks/{rack}', 'RacksController@update');
    Route::delete('/racks/{rack}', 'RacksController@destroy');

    Route::get('/books', 'BooksController@adminIndex');
    Route::post('/books', 'BooksController@store');
    Route::get('/books/{book}/edit', 'BooksController@edit');
    Route::patch('/books/{book}', 'BooksController@update');
    Route::delete('/books/{book}', 'BooksController@destroy');
});
