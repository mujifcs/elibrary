<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'author', 'pub_year',
    ];

    /**
     * Get the rack that has this book.
     */
    public function rack()
    {
        return $this->belongsTo('App\Rack');
    }
}
