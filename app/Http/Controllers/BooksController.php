<?php

namespace App\Http\Controllers;

use App\Rack;
use App\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BooksController extends Controller
{

    public function adminIndex(){
        $books = Book::all();
        $racks = Rack::all();

        return view('admin.books', compact('books', 'racks'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bookData = $request->validate([
            'title' => 'required|min:2|max:20|string',
            'author' => 'required|min:2|max:20|string',
            'pub_year' => 'integer|required|min:1901|max:2155|date_format:Y',
            'rack' => 'required|exists:racks,id'
        ]);

        $rack = Rack::find($bookData['rack']);

        if(!$rack->canAddBook()){
            return back()->withErrors(['error' => 'Rack: '.$rack->name.' is already full please add book in some other rack!'])->withInput();
        }

        $rack->books()->create($bookData);

        return redirect('admin/books')->with(['status' => 'Book created successfully!']);
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Rack  $rack
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Rack $rack, Book $book )
    {
        return view('books.show', compact('rack','book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $Book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        $racks = Rack::all();
        return view('admin.edit-book', compact('book', 'racks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        $bookData = $request->validate([
            'title' => 'required|min:2|max:20|string',
            'author' => 'required|min:2|max:20|string',
            'pub_year' => 'integer|required|min:1901|max:2155|date_format:Y',
            'rack' => 'required|exists:racks,id'
        ]);

        $book->update($bookData);

        $rack = Rack::find($bookData['rack']);

        if($book->rack_id!== $rack->id && !$rack->canAddBook()){
            return back()->withErrors(['error' => 'Rack: '.$rack->name.' is already full please add book in some other rack!'])->withInput();
        }

        $rack->books()->save($book);

        return back()->with(['status' => 'Book updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();
        return redirect('admin')->with(['status' => 'Book deleted successfully!']);
    }


    public function search(Request $request)
    {
        $data = $request->validate([
            'term' => 'string|min:2|required'
        ]);

        $term = $data['term'];

        $books = DB::table('books')
                ->where('title', 'like', '%'.$term.'%')
                ->orWhere('author', 'like', $term.'%')
                ->orWhereYear('pub_year', $term)
                ->get();


        return view('books.results', compact('books','term'));
    }
}
