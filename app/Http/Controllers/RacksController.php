<?php

namespace App\Http\Controllers;

use App\Rack;
use Illuminate\Http\Request;

class RacksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $racks = Rack::all();

        return view('racks.index', compact('racks'));
    }

    /**
     * Display a listing of the resource for admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function adminIndex()
    {
        $racks = Rack::all();

        return view('admin.racks', compact('racks'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rackData = $request->validate([
            'name' => 'required|min:2|max:20|unique:racks,name',
        ]);

        Rack::create($rackData);

        return redirect('admin/racks')->with(['status' => 'Rack created successfully!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rack  $rack
     * @return \Illuminate\Http\Response
     */
    public function show(Rack $rack)
    {
        return view('racks.show', compact('rack'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rack  $rack
     * @return \Illuminate\Http\Response
     */
    public function edit(Rack $rack)
    {
        return view('admin.edit-rack', compact('rack'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rack  $rack
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rack $rack)
    {
        $data = $request->validate([
            'name' => 'required|min:2|max:20|unique:racks,name',
        ]);

        $rack->name = $data['name'];

        $rack->save();

        return back()->with(['status' => 'Rack updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rack  $rack
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rack $rack)
    {
        $rack->delete();
        return redirect('admin/racks')->with(['status' => 'Rack deleted successfully!']);
    }
}
