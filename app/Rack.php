<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rack extends Model
{
    private $rackLimit = 10;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * The books that belong to the rack.
     */
    public function books()
    {
        return $this->hasMany('App\Book');
    }

    public function canAddBook(){
        return $this->books()->count() < $this->rackLimit;
    }
}
