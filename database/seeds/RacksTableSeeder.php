<?php

use Illuminate\Database\Seeder;
use App\Rack;

class RacksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i=0; $i < 5; $i++) {
            Rack::create([
                'name' => $faker->unique()->word
            ]);
        }

    }
}
