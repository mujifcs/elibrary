<?php

use Illuminate\Database\Seeder;
use App\Book;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        $rack = \App\Rack::first();

        for ($i=0; $i < 9; $i++) {
            Book::create([
                'title' => $faker->sentence,
                'author' => $faker->name,
                'pub_year' => $faker->year,
                'rack_id' => $rack->id
            ]);
        }
    }
}
